print ("Welcome to Calculator")

c = 1

while c == 1 :

	a1 = float(input("\nEnter the first value : "))
		
	b = input('''Select the Operator
 		+ to Add
 		- to Subtract
 		* to Multiply
 		/ to Divide\n''')
	
	a2 = float(input("Enter the second value : "))
		
	if b == '+' :
		print (a1 + a2)

	elif b == '-' :
		print (a1 - a2)

	elif b == '*' :
		print (a1 * a2)

	elif b == '/' :
		print (a1 / a2)

	else :
		d = input('''\nInvalid Choice !
			Press 1 to try Again
			Press 2 to exit\n''')

		if d == '1' :
			continue 

		elif d == '2' :
			print ("Good Bye...")
			c = 0

		else :
			print ("Invalid Input")

		